package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.dawan.springboot.enums.Contrat;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.Version;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

//Une entité doit :
//- être annoté avec @Entity
//- avoir un attribut qui représente la clé primaire annoté avec @Id
//- implémenté l'interface Serializable
//- avoir obligatoirement un contructeur par défaut
@Entity
@Table(name = "employes") // L'annotation @Table permet de modifier le nom de la table, sinon elle prend
                          // le nom de la classe
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id // -> Clé primaire simple
    @GeneratedValue(strategy = GenerationType.AUTO) // 1 - l'ORM choisi la stratégie
//  @GeneratedValue(strategy = GenerationType.IDENTITY) // 2 - BDD -> auto_increment, identity, serial ...
//    @TableGenerator(name = "tablegen")                // 3 - ORM
//    @GeneratedValue(strategy = GenerationType.TABLE, generator = "tablegen")
//    @SequenceGenerator(name="emp_seq")                // 4 -> BDD séquence
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="emp_seq")
    private long id;
    
    @Version
    private int version;

    // L'annotation @Column permet pour définir plus précisément la colonne
    @Column(length = 50)
    private String prenom; // l'attribut length permet modifier de la longueur d'une chaine de caractère ou
                           // d'un @lob

    // l'attribut nullable permet de définir si le champ peut être NULL par défaut true
    @Column(length = 50, nullable = false)
    private String nom;

    // Les types primitif sont implitement NOT NULL
    private double salaire;

    // Les attributs @Transient ne sont transient ne sont pas persister sinon tous
    // les autres sont par défaut persistant
    @Transient
    private int nePasPersiter;

    // LocalDate, LocalTime, LocalDateTime sont supportés par hibernate depuis la version 5
    // pas besoin de l'annotation @Temporal
    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance;

    @Column(unique = true, nullable = false)
    private String email;

    // Une énumération peut être stocké sous forme numérique EnumType.ORDINAL (par défaut)
    // ou sous forme de chaine de caractères EnumType.STRING
    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private Contrat contrat;

    @Lob // => pour stocker des données binaires dans la bdd (image, ...) BLOB ou un long texte CLOB
    @Column(length = 65000) // l'attribut length permet de définir la taille du blob => ici Blob
    private byte[] photo; // BLOB -> tableau de byte , CLOB -> String ou tableau de caractère

    // @ Embedded => pour utiliser une classe intégrable
    @Embedded
    private Adresse adressePerso;

    // Pour utiliser plusieurs fois la même classe imbriqué dans la même entitée
    // On aura plusieurs fois le même nom de colonne dans la table => erreur
    // On pourra renommé les colonnes avec des annotations @AttributeOverride placé
    // dans une annotation @AttributeOverrides
    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")),
            @AttributeOverride(name = "ville", column = @Column(name = "ville_pro")),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro", length = 15)) })
    private Adresse adressePro;
    
    @ElementCollection
    @CollectionTable(name="commentaires", joinColumns = @JoinColumn(name="id_employes"))
    @Column(name="commentaire",length = 200)
    private List<String> commentaires=new ArrayList<>();
    
    @ElementCollection
    @CollectionTable(name="telephones",joinColumns = @JoinColumn(name="id_employe"))
    @MapKeyColumn(name="type_telephone",length = 20)
    @Column(name="numero_telephones",length = 20)
    private Map<String, String> telephones=new HashMap<>();
}
