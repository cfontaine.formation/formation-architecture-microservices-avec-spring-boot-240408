package fr.dawan.springboot.entities;

import java.time.LocalDateTime;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseAuditing {

    @CreatedDate
    @Column(updatable = false) // nullable=false
    private LocalDateTime created;

    @LastModifiedDate
    @Column(name = "last_modified")
    private LocalDateTime lastModifed;

    @CreatedBy
    @Column(updatable = false, length = 100) // nullable=false
    private String createBy;

    @LastModifiedBy
    @Column(length = 100) // nullable=false
    private String modifiedBy;
}
