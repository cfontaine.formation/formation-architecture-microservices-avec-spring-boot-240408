package fr.dawan.springboot.entities.heritage;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
//Héritage => trois façons d’organiser l’héritage

//1 - SINGLE_TABLE => Le parent et les enfants vont être placé dans la même table
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//Une colonne "Discriminator" définit le type de la classe enregistrée
//@DiscriminatorColumn(name = "compte_discriminator")
//Pour la classe CompteBancaire la valeur dans la colonne discriminator => CB
//@DiscriminatorValue("CB")

//2 - TABLE_PER_CLASS => le parent et chaque enfant auront leur propre table
//la table enfant aura les colonnes des variables d'instance de la classe parent
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

//3 - JOINED => on aura une jointure entre la table de la classe parent et la table de la classe enfant
@Inheritance(strategy = InheritanceType.JOINED)

@Table(name = "compte_bancaires")
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    // 2 - GenerationType.IDENTITY ne fonction avec TABLE_PER_CLASS
    // @GeneratedValue(strategy = GenerationType.AUTO)

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 50, nullable = false)
    private String titulaire;

    private double solde;
}
