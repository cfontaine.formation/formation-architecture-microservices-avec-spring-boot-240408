package fr.dawan.springboot.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

//Une classe intégrable va stocker ses données dans la table de l’entité mère ce qui va créer des colonnes supplémentaires
@Embeddable // -> classe intégrable avec @Embeddable
public class Adresse {

    private String rue;

    private String ville;

    @Column(name = "code_postal", length = 15)
    private String codePostal;
}
