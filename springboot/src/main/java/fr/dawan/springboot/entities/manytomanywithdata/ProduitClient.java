package fr.dawan.springboot.entities.manytomanywithdata;

import java.io.Serializable;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "produit_client")
public class ProduitClient implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ProduitClientPk id = new ProduitClientPk();

    @ManyToOne
    @MapsId("produitId")
    @Exclude
    private Produit produit;

    @ManyToOne
    @MapsId("clientId")
    @Exclude
    private Client client;

    private int quantite;

    public ProduitClient(Produit produit, Client client, int quantite) {
        this.produit = produit;
        this.client = client;
        this.quantite = quantite;
    }
}
