package fr.dawan.springboot.entities.heritage;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "compte_epargnes")
//pour SINGLE_TABLE
//Pour la classe CompteEpargne la valeur dans la colonne discriminator => CE
// @DiscriminatorValue("CE")
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;

    @Column(name = "taux_interet")
    private double tauxInteret;
}
