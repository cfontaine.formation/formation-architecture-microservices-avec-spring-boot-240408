package fr.dawan.springboot.entities.heritage;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "films")
public class Film extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(length = 100)
    private String titre;

    @Column(name = "annee_sortie")
    private int anneeSortie;
}
