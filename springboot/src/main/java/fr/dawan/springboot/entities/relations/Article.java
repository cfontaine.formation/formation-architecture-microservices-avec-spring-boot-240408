package fr.dawan.springboot.entities.relations;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import fr.dawan.springboot.enums.Emballage;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "articles")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(nullable = false)
    private String description;

    private double prix;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private Emballage emballage;

    @ManyToOne
    /// @JoinColumn(name="id_marques")
    private Marque marque;

    // @ManyToMany
    // private List<Fournisseur> fournisseurs=new ArrayList<>();

    @ManyToMany
//     @JoinTable(name="article2fournisseur"
//     ,joinColumns = @JoinColumn(name="id_articles")
//     , inverseJoinColumns = @JoinColumn(name="id_fournisseurs"))

    @Exclude
    private Set<Fournisseur> fournisseurs = new HashSet<>();
}
