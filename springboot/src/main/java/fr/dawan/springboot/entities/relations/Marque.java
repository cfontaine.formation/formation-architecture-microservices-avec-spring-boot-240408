package fr.dawan.springboot.entities.relations;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import fr.dawan.springboot.entities.BaseAuditing;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.NamedSubgraph;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "marques")

@NamedEntityGraph(name = "marque_article_graph", 
                  attributeNodes = {
                          @NamedAttributeNode(value = "articles", subgraph = "sub_article_fournisseur"),
                          @NamedAttributeNode("charteGraphique") },
                  subgraphs = @NamedSubgraph(name = "sub_article_fournisseur", attributeNodes = @NamedAttributeNode("fournisseurs")))
public class Marque extends BaseAuditing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;

    @Column(length = 60, nullable = false)
    private String nom;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
    private CharteGraphique charteGraphique;

    // Unidirectionnel avec un @OneToMany (à éviter)
    // il vaut mieux utiliser un @ManyToOne sur l'autre entité
//    @OneToMany
//    @JoinColumn(name="marque_id")

    // En bidirectionnel
    @OneToMany(mappedBy = "marque", cascade = { CascadeType.REMOVE, CascadeType.REMOVE }) // ,fetch = FetchType.EAGER)
    @Exclude
    private Set<Article> articles = new HashSet<>();
}
