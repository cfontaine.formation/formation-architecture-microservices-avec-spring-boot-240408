package fr.dawan.springboot;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.entities.relations.Marque;

@SpringBootApplication
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);

//      à la place de l'éxécution de la méthode de classe run,
//      On peut créer une instance de SpringApplication et la personnaliser

//	    SpringApplication app=new SpringApplication(SpringbootApplication.class);
//	    app.setAddCommandLineProperties(false);
//	    app.setBannerMode(Banner.Mode.OFF);
//	    app.run(args);
    }

    @Bean
    ModelMapper modelmapper() {
        ModelMapper mapper = new ModelMapper();
        // setAmbiguityIgnored(true) ->
        // Par défaut, Lorsque ModdelMapper réalise le mapping, si les propriétés de
        // destination correspondent à plusieurs propriétés source
        /// il lance une exception :

        // org.modelmapper.ConfigurationException: ModelMapper configuration errors:
        // The destination property
        // fr.dawan.springboot.entities.relations.Marque.setCharteGraphique()/fr.dawan.springboot.entities.relations.CharteGraphique.setMarque()
        // matches multiple source property hierarchies:
        // fr.dawan.springboot.dtos.MarqueDto.getCharteGraphiqueCouleur()
        // fr.dawan.springboot.dtos.MarqueDto.getCharteGraphiqueLogo()
        // ici elle est provoqué par le mapping bidirectionnel dans les entités. Il
        // suffit de désactiver se comportement par défaut

        // Avec modelMapper, la méthode getConfiguration() permet de modifier la
        // configuration par défaut
        // voir -> http://modelmapper.org/user-manual/configuration/
        mapper.getConfiguration().setAmbiguityIgnored(true);
        // On indique à modelMapper comment faire le mapping la couleur et le logo dto -> entité
        mapper.typeMap(MarqueDto.class, Marque.class)
        .addMappings(m -> m.map(src -> src.getCharteGraphiqueCouleur(), (dest, v) -> dest.getCharteGraphique().setCouleur((String) v)))
        .addMappings(m -> m.map(src -> src.getCharteGraphiqueLogo(), (dest, v) -> dest.getCharteGraphique().setLogo((byte[]) v)));
        return mapper;
    }

}
