package fr.dawan.springboot.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    
    @Bean
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
        //return NoOpPasswordEncoder.getInstance();
    }

    
//    @Bean
//    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
//        return http.csrf(c-> c.disable())
//        .authorizeHttpRequests(auth -> auth.requestMatchers(
//                new AntPathRequestMatcher("/**","POST")).authenticated()
//                .requestMatchers(new AntPathRequestMatcher("/**","DELETE")).authenticated()
//                .requestMatchers(new AntPathRequestMatcher("/**","PUT")).authenticated()
//                .anyRequest().permitAll())
//        .httpBasic(Customizer.withDefaults()).build();    
//    }
    
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http.csrf(c-> c.disable())
        .authorizeHttpRequests(auth -> auth.requestMatchers(
                new AntPathRequestMatcher("/actuator/health","GET")).permitAll()
                .requestMatchers(new AntPathRequestMatcher("/actuator/","GET")).hasAnyAuthority("ADMIN")
                .requestMatchers(new AntPathRequestMatcher("/**","POST")).hasAnyAuthority("WRITE","ADMIN")
                .requestMatchers(new AntPathRequestMatcher("/**","DELETE")).hasAnyAuthority("DELETE","ADMIN")
                .requestMatchers(new AntPathRequestMatcher("/**","PUT")).hasAnyAuthority("WRITE","ADMIN")
                .requestMatchers(new AntPathRequestMatcher("/**","GET")).hasAnyAuthority("READ","ADMIN")
                .anyRequest().permitAll())
        .httpBasic(Customizer.withDefaults()).build();    
    }
}
