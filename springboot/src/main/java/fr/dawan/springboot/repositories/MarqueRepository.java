package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.dawan.springboot.entities.relations.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {
    @EntityGraph(value = "marque_article_graph") // ,type = EntityGraphType.FETCH)

    // @EntityGraph(attributePaths = {"articles","charteGraphique"})//,type=EntityGraphType.LOAD)
    List<Marque> findByNom(String nomMarque);

    @Query("SELECT m FROM Marque m JOIN FETCH m.articles WHERE m.nom=:nomMarque")
    List<Marque> findByNomJPQL(String nomMarque);

    int removeById(long id);
}
