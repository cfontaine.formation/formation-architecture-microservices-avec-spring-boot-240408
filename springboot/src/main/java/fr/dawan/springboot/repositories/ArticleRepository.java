package fr.dawan.springboot.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import fr.dawan.springboot.entities.relations.Article;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.enums.Emballage;

// @Repository // -> implicite
// @Transactional
public interface ArticleRepository extends JpaRepository<Article, Long> {

    // sujet find
    List<Article> findByPrix(double prixUnitaire);

    List<Article> findByPrixLessThan(double prixMax);

    List<Article> findByEmballageAndPrixGreaterThan(Emballage emballage, double prixMin);

    List<Article> findByPrixBetween(double prixMin, double prixMax);

    List<Article> findByEmballageIn(Emballage... emballages);

    List<Article> findByDescriptionLike(String modele);

    List<Article> findByDescriptionIgnoreCase(String description);

    List<Article> findAllByOrderByPrixDesc();

    List<Article> findByPrixLessThanOrderByDescriptionDescPrix(double prixMax);

    List<Article> findByMarque(Marque marque);

    List<Article> findByMarqueNom(String nomMarque);

    List<Article> findByMarqueDateCreationAfter(LocalDate creation);

    List<Article> findByMarqueCharteGraphiqueCouleur(String couleur);

    List<Article> findByFournisseursNom(String nomFournisseur);

    Article findTopByOrderByPrixDesc();

    List<Article> findTop5ByOrderByPrix();

    // Sujet exist -> type retour boolean
    boolean existsByEmballage(Emballage emballage);

    // Sujet count -> type retour int
    int countByPrixGreaterThan(double prixMin);

    // sujet delete ->type retour void ou int (le nombre d'entité supprimée)
    void deleteByMarque(Marque m);

    int removeById(long id);

    // Pagination
    Page<Article> findAllBy(Pageable page);

    List<Article> findByPrixLessThan(double prixMax, Pageable page);

    List<Article> findByPrixLessThan(double prixMax, Sort sort);

    // JPQL
    @Query("SELECT a FROM Article a WHERE a.prix<:prix")
    List<Article> findJPQL(@Param("prix") double prixMax);

    @Query("FROM Article a WHERE a.prix<:prixMax")
    List<Article> findJPQL2(double prixMax);

    @Query("SELECT a FROM Article a WHERE a.prix>?1 AND a.emballage=?2")
    List<Article> findJPQL3(double prixMin, Emballage emballage);

    // Expression de chemin -> uniquement @OneToOne et @ManyToOne
    @Query("SELECT a FROM Article a WHERE a.marque.nom=:nomMarque")
    List<Article> findJPQL4(String nomMarque);

    // @ManyToMany -> on ne peut pas utiliser l'expression de chemin
    // @Query("SELECT a FROM Article a WHERE a.fournisseurs.nom=:nomFournisseur")
    @Query("SELECT a FROM Article a JOIN a.fournisseurs f WHERE f.nom=:nomFournisseur")
    List<Article> findJPQL5(String nomFournisseur);

    @Query("SELECT count(a) FROM Article a WHERE a.prix<:prixMax GROUP BY a.emballage")
    List<Integer> findJPQL6(double prixMax);

    @Query("SELECT sum(a.prix) FROM Article a GROUP BY a.emballage")
    List<Double> findJPQL7();

    // SQL
    @Query(value = "SELECT * FROM articles WHERE prix<:prixMax", nativeQuery = true)
    List<Article> findSQL(double prixMax);

    // Procédure stockée
    @Procedure("get_count_by_prix")
    // int countInferieurPrix(@Param("montant") double prixMax);
    int countInferieurPrix(double montant);

    @Procedure
    int get_count_by_prix(@Param("montant") double prixMax);
}
