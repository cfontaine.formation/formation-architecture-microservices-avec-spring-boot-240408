package fr.dawan.springboot.repositories;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.relations.Marque;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;

@Repository
public class MarqueCustomRepositoryImpl implements MarqueCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Marque> findBy(String nom, LocalDate creation) {
        String requete = "SELECT m FROM Marque m";
        Map<String, Object> params = new HashMap<>();
        if (nom != null) {
            requete += " WHERE m.nom=:nom";
            params.put("nom", nom);
        }
        if (creation != null) {
            if (requete.contains("WHERE")) {
                requete += " AND m.dateCreation=:creation";
            } else {
                requete += " WHERE m.dateCreation=:creation";
            }
            params.put("creation", creation);
        }

        TypedQuery<Marque> tq = em.createQuery(requete, Marque.class);
        for (Entry<String, Object> en : params.entrySet()) {
            tq.setParameter(en.getKey(), en.getValue());
        }
        return tq.getResultList();
    }

}
