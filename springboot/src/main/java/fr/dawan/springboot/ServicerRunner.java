package fr.dawan.springboot;

import java.time.LocalDate;

import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dtos.ArticleDto;
import fr.dawan.springboot.dtos.ArticleDto2;
import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.entities.relations.Article;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueService;
import jakarta.validation.ConstraintViolationException;

//@Component
//@Order(1)
public class ServicerRunner implements CommandLineRunner {

    @Autowired
    private ArticleRepository repository;

    @Autowired
    private MarqueRepository marqueRepository;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private MarqueService service;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Service Runner");

        // ModdelMapper
        Article a = repository.findById(2L).get();
        System.out.println(a);
        ArticleDto dto = mapper.map(a, ArticleDto.class);
        System.out.println(dto);

        ArticleDto2 dto2 = mapper.map(a, ArticleDto2.class);
        System.out.println(dto2);
        mapper.typeMap(Article.class, ArticleDto2.class).addMappings(
                m -> m.map(src -> src.getMarque().getNom(), (dest, v) -> dest.setIntituleMarque((String) v)));
        dto2 = mapper.map(a, ArticleDto2.class);
        System.out.println(dto2);

        Provider<Marque> prodiver = n -> marqueRepository.findByNom(((String) n.getSource())).get(0);
        mapper.typeMap(ArticleDto2.class, Article.class).addMappings(
                m -> m.with(prodiver).map(src -> src.getIntituleMarque(), (dest, v) -> dest.setMarque((Marque) v)));
        Article a2 = mapper.map(dto2, Article.class);
        System.out.println(a2);
        System.out.println(a2.getMarque());

        // Service
        service.getAllMarque(Pageable.unpaged()).forEach(m -> System.out.println(m));
        System.out.println("------------------------------------");
        MarqueDto marqueDto = service.save(new MarqueDto(0, "Marque M", LocalDate.of(1997, 10, 10), "FF0000", null));
        System.out.println(marqueDto);
        long id = dto.getId();
        System.out.println("------------------------------------");
        marqueDto.setNom("Marque L1");
        marqueDto = service.update(marqueDto, id).get();
        System.out.println(marqueDto);
        System.out.println("------------------------------------");
        marqueDto = service.getMarqueById(id).get();
        System.out.println(marqueDto);
        System.out.println("------------------------------------");
        System.out.println(service.deleteMarque(id));
        System.out.println("------------------------------------");
        System.out.println(service.deleteMarque(id));

        // validation
        try {
            service.save(new MarqueDto(0, "Marque M", LocalDate.of(2997, 10, 10), "FF0000", null));
        } catch (ConstraintViolationException e) {
            System.err.println(e.getMessage());
        }
    }

}
