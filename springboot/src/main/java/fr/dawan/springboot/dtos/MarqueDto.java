package fr.dawan.springboot.dtos;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class MarqueDto {

    // @Min(value = 0, message="id>0")
    @Min(0)
    private long id;

    @NotNull
    @Size(max = 60)
    private String nom;

    @PastOrPresent
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate dateCreation;

    @Size(min = 6, max = 6)
    @Pattern(regexp = "[0-9a-fA-F]+")
    @JsonProperty("couleur")
    private String charteGraphiqueCouleur;

    @Exclude
    @JsonProperty("logo")
    private byte[] charteGraphiqueLogo;
}
