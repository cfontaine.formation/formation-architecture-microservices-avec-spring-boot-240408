package fr.dawan.springboot.dtos;

import fr.dawan.springboot.enums.Emballage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class ArticleDto {

    private long id;

    private double prix;

    private String description;

    private Emballage emballage;

    private long idMarque;

}
