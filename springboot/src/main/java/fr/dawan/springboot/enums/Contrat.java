package fr.dawan.springboot.enums;

public enum Contrat {
    CDI, CDD, INTERIM, APPRENTI, STAGIAIRE
}
