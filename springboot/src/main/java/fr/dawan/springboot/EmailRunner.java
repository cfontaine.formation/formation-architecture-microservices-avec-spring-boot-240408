package fr.dawan.springboot;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import fr.dawan.springboot.services.EmailService;

//@Component
//@Order(4)
// Pour tester les email, il faut un serveur stmp.
// le plus simple et d'utiliser fakesmtp qui va simulé un serveur smtp en local et par défaut sur le port 250
// sinon on peut utiliser un compte gmail limiter à 100 email/ jour
public class EmailRunner implements ApplicationRunner {
  @Autowired
  private EmailService emailService;

  @Override
  public void run(ApplicationArguments args) throws Exception {
      emailService.sendSimpleMail("Un email de test", "test", "jd@dawan.com", "no-reply@dawan.com");
      
      emailService.sendHTMLMail("<html><body><h1>email HTML</h1></body></html>", "test", "jd@dawan.com", "no-reply@dawan.com");
      
      Map<String, Object> model = new HashMap<>();
      model.put("prenom", "John");
      model.put("email", "jdoe@dawan.com");
      emailService.sendTemplateMail("mailTemplate.ftlh", model, "un uml de test", "jd@dawan.com", "no-reply@dawan.com");

      emailService.sendMailAttachement("test mail", "test", "jd@dawan.com", "no-reply@dawan.com", new File("logo.jpg"));
  }

}

