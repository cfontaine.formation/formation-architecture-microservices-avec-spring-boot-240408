package fr.dawan.springboot.services.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.entities.relations.CharteGraphique;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.MarqueService;
import jakarta.validation.Valid;

@Service
@Transactional
@Validated
public class MarqueServiceImpl implements MarqueService {

    @Autowired
    private MarqueRepository repository;

    @Autowired
    private ModelMapper mapper;

    @Override
    public List<MarqueDto> getAllMarque(Pageable page) {
//          List<Marque> lst=repository.findAll(page).getContent();
//          List<MarqueDto> lstDto=new ArrayList<>();
//          for(Marque m: lst) {
//              lstDto.add(mapper.map(m, MarqueDto.class));
//          }
//          return lstDto;
        Page<Marque> pm = repository.findAll(page);
        return pm.stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    public Optional<MarqueDto> getMarqueById(long id) {
        return repository.findById(id).map(a -> Optional.of(mapper.map(a, MarqueDto.class))).orElse(Optional.empty());
    }

    @Override
    public List<MarqueDto> getMarqueByNom(String nom) {
        List<Marque> lst = repository.findByNom(nom);
        return lst.stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    public boolean deleteMarque(long id) {
        return repository.removeById(id) != 0;
    }

    @Override
    public MarqueDto save(@Valid MarqueDto marqueDto) {
        Marque m = repository.saveAndFlush(mapper.map(marqueDto, Marque.class));
        return mapper.map(m, MarqueDto.class);
    }

    @Override
    public Optional<MarqueDto> update(@Valid MarqueDto marqueDto, long id) {
        return repository.findById(id).map(m ->{
    
            m.setNom(marqueDto.getNom());
            m.setDateCreation(marqueDto.getDateCreation());
            if (m.getCharteGraphique() == null) {
                m.setCharteGraphique(new CharteGraphique());
            }
        m.getCharteGraphique().setCouleur(marqueDto.getCharteGraphiqueCouleur());
        m.getCharteGraphique().setLogo(marqueDto.getCharteGraphiqueLogo());
        return Optional.of(mapper.map(repository.saveAndFlush(m), MarqueDto.class));
        }).orElse(Optional.empty());
    }

}
