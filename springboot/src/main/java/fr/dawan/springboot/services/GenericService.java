package fr.dawan.springboot.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenericService<TDto, ID> {

    Page<TDto> getAll(Pageable page);

    TDto getById(ID id);

    boolean delete(ID id);

    TDto save(TDto dto);

    TDto update(TDto dto, ID id);
}