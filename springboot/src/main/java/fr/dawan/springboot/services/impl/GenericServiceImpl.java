package fr.dawan.springboot.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.services.GenericService;

public abstract class GenericServiceImpl<TDto, T, ID> implements GenericService<TDto, ID> {

    private JpaRepository<T, ID> repository;

    protected ModelMapper mapper;

    private Class<T> clazz;

    private Class<TDto> clazzDto;

    public GenericServiceImpl(JpaRepository<T, ID> repository, ModelMapper mapper, Class<T> clazz, Class<TDto> clazzDto) {
        this.repository = repository;
        this.mapper = mapper;
        this.clazz = clazz;
        this.clazzDto = clazzDto;
    }

    @Override
    public Page<TDto> getAll(Pageable page) {
        Page<T> p = repository.findAll(page);
        List<TDto> lst = p.getContent().stream().map(e -> mapper.map(e, clazzDto)).toList();
        return new PageImpl<>(lst, p.getPageable(), p.getTotalElements());
    }

    @Override
    public TDto getById(ID id) {
        return mapper.map(repository.findById(id), clazzDto);
    }

    @Transactional
    @Override
    public boolean delete(ID id) {
        repository.deleteById(id);
        return true;
    }

    @Transactional
    @Override
    public TDto save(TDto dto) {
        return mapper.map(repository.saveAndFlush(mapper.map(dto, clazz)), clazzDto);
    }

    @Transactional
    @Override
    public TDto update(TDto dto, ID id) {
        T e = repository.findById(id).get();
        updateEntity(e, dto);
        return mapper.map(repository.saveAndFlush(e), clazzDto);
    }

    protected abstract void updateEntity(T entity, TDto dto);

}
