package fr.dawan.springboot.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dtos.MarqueDto;
import jakarta.validation.Valid;

public interface MarqueService {

    List<MarqueDto> getAllMarque(Pageable page);

    Optional<MarqueDto> getMarqueById(long id);

    List<MarqueDto> getMarqueByNom(String nom);

    boolean deleteMarque(long id);

    MarqueDto save(@Valid MarqueDto marqueDto);

    Optional<MarqueDto> update(@Valid MarqueDto marqueDto, long id);

}
