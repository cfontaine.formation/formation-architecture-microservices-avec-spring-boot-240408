package fr.dawan.springboot.controllers;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.MarqueService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/v1/marques")
@Tag(name = "Marques", description = "L'api des marques")
public class MarqueController {

    @Autowired
    private MarqueService service;

    @Autowired
    private ObjectMapper objectMapper;

//    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
//    public List<MarqueDto>getAll() {
//        return service.getAllMarque(Pageable.unpaged());
//    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE/* ,params= {"page","size"} */)
    public List<MarqueDto> getAll(Pageable page) {
        return service.getAllMarque(page);
    }

    @Operation(summary = "Trouver les marques en fonction de leur id", description = "retourne une marque", tags = "Marques")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Opération réussit", content = @Content(schema = @Schema(implementation = MarqueDto.class))),
            @ApiResponse(responseCode = "404", description = "Marque non trouvé", content = @Content(schema = @Schema(implementation = String.class))) })
    @GetMapping(value = "/{id:[0-9]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> getById(
            @Parameter(description = "L'id de la marque", required = true, allowEmptyValue = false) @PathVariable long id) {
        try {
            return ResponseEntity.ok(service.getMarqueById(id).get());
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/{nom:[a-zA-Z]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> getByNom(@PathVariable String nom) {
        return service.getMarqueByNom(nom);
    }

    @GetMapping(params = "nom", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MarqueDto> getByNomParam(@RequestParam String nom) {
        return service.getMarqueByNom(nom);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteById(@PathVariable long id) {
        if (service.deleteMarque(id)) {
            return new ResponseEntity<>("La marque " + id + " est supprimée", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("La marque " + id + " n'existe pas", HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto create(@RequestBody MarqueDto marque) {
        return service.save(marque);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> update(@RequestBody MarqueDto marqueDto, @PathVariable long id) {
        return service.update(marqueDto, id)
                .map(marque -> ResponseEntity.ok(marque))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping(value = "/logo/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> uploadImage(@PathVariable long id, @RequestParam("image") MultipartFile file)
            throws IOException {
        MarqueDto dto;
        try {
            dto = service.getMarqueById(id).get();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
        System.out.println(file.getSize());
        System.out.println(file.getOriginalFilename());
        dto.setCharteGraphiqueLogo(file.getBytes());
        dto.setCharteGraphiqueCouleur("F20000");
        return ResponseEntity.ok(service.update(dto, id).get());
    }

    @PostMapping(value = "/marquelogo", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MarqueDto> uploadImage(@RequestParam("marque") String marqueDtoStr,
            @RequestParam("image") MultipartFile file) throws Exception {
        MarqueDto dto = objectMapper.readValue(marqueDtoStr, MarqueDto.class);
        System.out.println("dto=" + dto);
        dto.setCharteGraphiqueLogo(file.getBytes());
        dto.setCharteGraphiqueCouleur("F20000");
        return ResponseEntity.ok(service.save(dto));
    }

    @GetMapping("/ioexception")
    public void genIOException() throws IOException {
        throw new IOException("Erreur E/S");
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handlerIOException(IOException e) {
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
