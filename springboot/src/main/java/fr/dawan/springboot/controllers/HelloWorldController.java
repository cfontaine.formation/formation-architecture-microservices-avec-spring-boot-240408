package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.HelloProperties;

@RestController
public class HelloWorldController {
    
//    @Value("${message.hello : message par défaut}")
//    private String message;

    @Autowired
    private HelloProperties prop;
    
    @RequestMapping("/hello")
    public String hello() {
      //  return message;
     return prop.toString();
    }

}
