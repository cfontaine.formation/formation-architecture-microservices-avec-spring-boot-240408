package fr.dawan.springboot;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import fr.dawan.springboot.entities.relations.Article;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.enums.Emballage;
import fr.dawan.springboot.repositories.ArticleRepository;
import fr.dawan.springboot.repositories.MarqueCustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;

//@Component
//@Order(1)
public class RepositoryRunner implements CommandLineRunner {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private MarqueRepository marqueRepository;

    @Autowired
    private MarqueCustomRepository customRepository;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("RepositoryRunner");
        List<Article> articles = articleRepository.findAll();
        for (Article a : articles) {
            System.out.println(a);
        }
        System.out.println("-------------------------------------------");
        articleRepository.findByPrix(40.0).forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByPrixLessThan(100.0).forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByEmballageAndPrixGreaterThan(Emballage.CARTON, 200.0)
                .forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByPrixBetween(15.0, 40.0).forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByEmballageIn(Emballage.CARTON, Emballage.PLASTIQUE).forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByDescriptionLike("m%").forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByDescriptionLike("c___e%").forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findAllByOrderByPrixDesc().forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByPrixLessThanOrderByDescriptionDescPrix(300.0).forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByMarqueNom("Marque A").forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        articleRepository.findByFournisseursNom("Fournisseur 2").forEach(a -> System.out.println(a));

        System.out.println("-------------------------------------------");
        System.out.println(articleRepository.findTopByOrderByPrixDesc());

        System.out.println("-------------------------------------------");
        articleRepository.findTop5ByOrderByPrix().forEach(a -> System.out.println(a));

        // Pagination et trie
        // Pagination
        Page<Article> p = articleRepository.findAllBy(PageRequest.of(0, 3));
        System.out.println("Nombre d'élément de la page " + p.getNumberOfElements());
        System.out.println("Nombre d'élément par page " + p.getSize());
        System.out.println("Numéro de page " + p.getNumber());
        System.out.println("Nombre d'élément total " + p.getTotalElements());
        System.out.println("Nombre total de page " + p.getTotalPages());
        p.getContent().forEach(a -> System.out.println(a));

        articleRepository.findByPrixLessThan(100.0, PageRequest.of(0, 3)).forEach(a -> System.out.println(a));

        articleRepository.findByPrixLessThan(100.0, Pageable.unpaged()).forEach(a -> System.out.println(a));

        // Trie
        Sort sort = Sort.by("description").and(Sort.by(Direction.DESC, "prix"));
        articleRepository.findByPrixLessThan(100.0, PageRequest.of(0, 6, sort)).forEach(a -> System.out.println(a));

        articleRepository.findByPrixLessThan(800.0, sort).forEach(a -> System.out.println(a));

        // JPQL
        articleRepository.findJPQL(200.0).forEach(a -> System.out.println(a));

        articleRepository.findJPQL4("Marque A").forEach(a -> System.out.println(a));

        articleRepository.findJPQL5("Fournisseur 2").forEach(a -> System.out.println(a));

        articleRepository.findJPQL6(150.0).forEach(a -> System.out.println(a));

        articleRepository.findJPQL7().forEach(a -> System.out.println(a));

        // SQL
        articleRepository.findSQL(200.0).forEach(a -> System.out.println(a));

        System.out.println(articleRepository.existsByEmballage(Emballage.CARTON));

        System.out.println(articleRepository.countByPrixGreaterThan(80.0));

        // Lazy Loading
        List<Marque> lstMarque = marqueRepository.findByNomJPQL("Marque A");
        System.out.println(lstMarque);
        Marque mA = lstMarque.get(0);
        mA.getArticles().forEach(a -> {

            System.out.println(a);
            // System.out.println(a.getFournisseurs());
        });
        System.out.println(articleRepository.countInferieurPrix(80.0));
        System.out.println(articleRepository.get_count_by_prix(80.0));

        // repository personalisé
        customRepository.findBy("Marque A", null).forEach(m -> System.out.println(m));
        customRepository.findBy(null, LocalDate.of(1954, 10, 23)).forEach(m -> System.out.println(m));
        customRepository.findBy("Marque B", LocalDate.of(1954, 10, 23)).forEach(m -> System.out.println(m));

        // Auditing
        Marque mD = new Marque();
        mD.setNom("Marque D");
        mD.setDateCreation(LocalDate.now());
        marqueRepository.saveAndFlush(mD);
        long id = mD.getId();

        Marque mE = marqueRepository.findById(id).get();
        mE.setNom("Marque E");
        marqueRepository.saveAndFlush(mE);
    }
}
