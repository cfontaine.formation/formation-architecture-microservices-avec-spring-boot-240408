package fr.dawan.springboot.repositories;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.springboot.entities.relations.Marque;

//Test d'intégration du repository
//On test le repository de manière isolé

//Test unitaire
//- le plugin maven surefire permet d'éxécuter les tests unitaire
//- avec maven les méthodes qui finissent avec Test sont considérées comme des tests unitaires
//- pour éxécuter les tests unitaire avec maven => mvn test
//                                avec eclipse => run as junit test

//La base de donnée H2 est initialisée avec Data-TEST.sql
//La base peut être initialisé avec Data-(nom profil).sql

@DataJpaTest // ne crée et ne configure que la partie de l'application qui conserne la persistence
@ActiveProfiles("TEST") // On active le profil TEST
public class MarqueRepositoryTest {

    @Autowired
    MarqueRepository repo;

    // Avec Junit, chaque test est une méthode annotée avec @Test
    @Test
    public void repositotyTest() {
        // On teste si le repository est bien injecté
        assertNotNull(repo);
    }

    // Chaque test est éxécuté dans une transaction et à la fin du test un rollback est éxécuté
    // Toutes le modifications de la base de donnnées qui ont eu lieu pendant le test sont annulées
    @Test
    public void findByNom() {
        Marque ma = new Marque();
        ma.setId(1L);
        ma.setNom("MarqueA");
        ma.setDateCreation(LocalDate.of(1923, 9, 10));

        // On excute la méthode à tester => findByNom("marqueA")
        List<Marque> lst = repo.findByNom("MarqueA");
        // On teste si on obtient le résultat attendu, sinon le test échoue
        assertEquals(1, lst.size()); // on test si l'on obtient 2 marques
        // On utilise la bibliothèque hamcrest assertion: assertThat avec le matcher samePropertyValuesAs
        // pour vérifier que chaque variable d'instance des objets obtenues
        // correspondent à celles des objets attendues
        assertThat(ma, Matchers.samePropertyValuesAs(lst.get(0)));
    }

}