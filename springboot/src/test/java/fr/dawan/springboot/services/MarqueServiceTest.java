package fr.dawan.springboot.services;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.entities.relations.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.impl.MarqueServiceImpl;

//Test unitaire de MarqueService

@ExtendWith(MockitoExtension.class) // pour utiliser Mockito avec Junit 5
public class MarqueServiceTest {

 // @Mock On crée un mock pour MarqueRepository et ModelMapper
 // Mockito va créer un Mock, un objet qui est une doublure de l'objet réel
 // mais qui ne contient la logique de l'objet
 @Mock
 private MarqueRepository repository;

 @Mock
 private ModelMapper mapper;

 // On injecte les mocks (au lieu des objets réels) dans le service
 @InjectMocks
 private MarqueServiceImpl service;

 private Marque marque;

 private MarqueDto marqueDto;

 // Avant chaque test, la méthode annotée avec @BeforeEach est éxécutée
 // Ici, on recrée les objets marque et marqueDto avant chaque test
 @BeforeEach
 void setUp() {
     marque = new Marque();
     marque.setId(1L);
     marque.setNom("Marque A");
     marque.setDateCreation(LocalDate.of(2024, 4, 16));
     marqueDto = new MarqueDto(1L, "Marque A", LocalDate.of(2024, 4, 16),null,null);
 }

 @Test
 void getAllMarqueTest() {
     List<Marque> lst = new ArrayList<>();
     lst.add(marque);
     PageImpl<Marque> page = new PageImpl<Marque>(lst, Pageable.unpaged(), 1);
     // On va indiquer au mock :
     // - quand la méthode findAll du repository est appelée avec en paramètre
     // Pageable.unpaged(), on retourne l'objet page
     when(repository.findAll(Pageable.unpaged())).thenReturn(page);
     // - quand la méthode map de modelMapper est appelée avec en paramètre marque, on retourne marqueDto
     when(mapper.map(marque, MarqueDto.class)).thenReturn(marqueDto);
     // On exécute la méthode getAllMarque du service
     List<MarqueDto> lstDto = service.getAllMarque(Pageable.unpaged());
     // On vérifie si le résultat obtenu, correspond à ce qui est attendu
     assertEquals(1, lstDto.size());
     assertThat(marqueDto, samePropertyValuesAs(lstDto.get(0)));
 }

 @Test
 void getMarqueByIdTest() {
      when(repository.findById(1L)).thenReturn(Optional.of(marque));
      when(mapper.map(marque, MarqueDto.class)).thenReturn(marqueDto);
      assertThat(Optional.of(marqueDto),samePropertyValuesAs(service.getMarqueById(1L)));
 }

 @Test
 void getMarqueByNameTest() {
     List<Marque> lst = new ArrayList<>();
     lst.add(marque);
     when(repository.findByNom("Marque")).thenReturn(lst);
     when(mapper.map(marque, MarqueDto.class)).thenReturn(marqueDto);
     List<MarqueDto> lstDto = service.getMarqueByNom("Marque");
     assertEquals(1, lstDto.size());
     assertThat(marqueDto, samePropertyValuesAs(lstDto.get(0)));
 }

 @Test
 void deleteMarqueTest() {
     when(repository.removeById(1L)).thenReturn(1);
     when(repository.removeById(100L)).thenReturn(0);
     assertTrue(service.deleteMarque(1L));
     assertFalse(service.deleteMarque(100L));
 }
}
