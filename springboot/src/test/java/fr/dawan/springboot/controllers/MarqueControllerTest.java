package fr.dawan.springboot.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.json.JSONException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import fr.dawan.springboot.dtos.MarqueDto;
import fr.dawan.springboot.services.MarqueService;

@WebMvcTest(MarqueController.class)
@ActiveProfiles("TEST")
@AutoConfigureMockMvc(addFilters = false)
public class MarqueControllerTest {

    @MockBean
    private MarqueService service;

    @Autowired
    private MockMvc mockMvc;

    private static ObjectMapper objectMapper;
  
    @BeforeAll
    public static void setUp() {
        objectMapper = JsonMapper.builder() // Pour le support des localDate par jackson
                .addModule(new JavaTimeModule())
                .build();
    }

    @Test
    public void getAllMarqueTest() throws Exception {
        List<MarqueDto> lstDto = new ArrayList<>();
        lstDto.add(new MarqueDto(1L, "marque A",LocalDate.of(2024, 4, 16),null,null));
        lstDto.add(new MarqueDto(2L, "marque B",LocalDate.of(2024, 4, 16),null,null));
        when(service.getAllMarque(any())).thenReturn(lstDto);
        MockHttpServletResponse reponse =  mockMvc.perform(
                get("/api/v1/marques").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn().getResponse();
        assertEquals(200,reponse.getStatus());
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"marque A\",\"dateCreation\":\"16-04-2024\",\"couleur\":null,\"logo\":null},{\"id\":2,\"nom\":\"marque B\",\"dateCreation\":\"16-04-2024\",\"couleur\":null,\"logo\":null}]", reponse.getContentAsString(), false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    public void getMarqueByIdTest() throws Exception {
        MarqueDto mDto = new MarqueDto(42L, "Marque A",LocalDate.of(2024, 4, 16),null,null);
        when(service.getMarqueById(42L)).thenReturn(Optional.of(mDto));
        MockHttpServletResponse reponse= mockMvc.perform(get("/api/v1/marques/42").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse();
        assertEquals(200,reponse.getStatus());
        try {
            JSONAssert.assertEquals("{\"id\":42,\"nom\":\"Marque A\",\"dateCreation\":\"16-04-2024\",\"couleur\":null,\"logo\":null}", reponse.getContentAsString(), JSONCompareMode.STRICT);
        } catch (JSONException e) {
            fail();
        }
    }
    
    @Test
    public void getMarqueByIdFailTest() throws Exception {
        when(service.getMarqueById(3000L)).thenThrow(new NoSuchElementException());
        int statusCode= mockMvc.perform(get("/api/v1/marques/3000").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse().getStatus();
        assertEquals(404,statusCode);
    }

    @Test
    void getMarqueByNameTest() throws Exception {
        List<MarqueDto> lstDto = new ArrayList<>();
        lstDto.add(new MarqueDto(1L, "marqueA",LocalDate.of(2024, 4, 16),null,null));
        when(service.getMarqueByNom("marqueA")).thenReturn(lstDto);
        MockHttpServletResponse reponse = mockMvc.perform(get("/api/v1/marques/marqueA")).andReturn().getResponse();
        assertEquals(200, reponse.getStatus());
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"marqueA\"}]",reponse.getContentAsString(),false);
        } catch (JSONException e) {
            fail();
        }
    }
    
    @Test
    public void deleteMarqueByIdTest() throws Exception {
     when(service.deleteMarque(1L)).thenReturn(true);
     when(service.deleteMarque(2L)).thenReturn(false);
     int statusOk=mockMvc.perform(delete("/api/v1/marques/1")).andReturn().getResponse().getStatus();
     int status404=mockMvc.perform(delete("/api/v1/marques/2")).andReturn().getResponse().getStatus();
     assertEquals(200, statusOk);
     assertEquals(404,status404);
    }

    @Test
    public void createMarqueTest() throws UnsupportedEncodingException, Exception {
        MarqueDto dto = new MarqueDto(0L, "Marque A",LocalDate.of(2024, 4, 16),null,null);
        when(service.save(any(MarqueDto.class))).thenReturn(new MarqueDto(1L, "Marque A",LocalDate.of(2024, 4, 16),null,null));

       MockHttpServletResponse reponse = mockMvc
                .perform(post("/api/v1/marques").contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(dto)))
                        .andReturn().getResponse();
       assertEquals(201, reponse.getStatus());
        try {
            JSONAssert.assertEquals("{\"id\":1,\"nom\":\"Marque A\",\"dateCreation\":\"16-04-2024\"}", reponse.getContentAsString(), false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    public void updateMarqueTest() throws JsonProcessingException, UnsupportedEncodingException, Exception {
        MarqueDto dto = new MarqueDto(2L, "Marque BBB",LocalDate.of(2024, 4, 16),null,null);
        when(service.getMarqueById(2L)).thenReturn(Optional.of(new MarqueDto(2L,"Marque B",LocalDate.of(2024, 4, 16),null,null)));
        when(service.update(any(MarqueDto.class),anyLong()))
        .thenReturn(Optional.of(new MarqueDto(2L, "Marque BBB",LocalDate.of(2024, 4, 16),null,null)));
        MockHttpServletResponse reponse = mockMvc
                .perform(put("/api/v1/marques/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andReturn().getResponse();
        assertEquals(200, reponse.getStatus());
        try {
            JSONAssert.assertEquals("{\"id\":2,\"nom\":\"Marque BBB\",\"dateCreation\":\"16-04-2024\"}", reponse.getContentAsString(), false);
        } catch (JSONException e) {
            fail();
        }      
    }
}
    

