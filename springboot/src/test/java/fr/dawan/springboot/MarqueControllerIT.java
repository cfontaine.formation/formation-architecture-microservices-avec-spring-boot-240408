package fr.dawan.springboot;
// Test d'intégration
// - le plugin maven failsafe permet  d'éxécuter les tests d'intégrations
// - avec maven les méthodes qui finissent avec IT sont considérées comme des tests d'intégrations
// - pour éxécuter avec maven uniquement les tests d'intégrations
//   mvn failsafe::integration-test



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.dawan.springboot.controllers.MarqueController;
import fr.dawan.springboot.dtos.MarqueDto;

@SpringBootTest
@ActiveProfiles("TEST")
@AutoConfigureMockMvc(addFilters = false)
@TestInstance(Lifecycle.PER_CLASS)
public class MarqueControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MarqueController marqueController;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void contextLoadsIT() {
        assertNotNull(marqueController);
    }

    @Test
    void getAllMarqueIT() throws Exception {
        MockHttpServletResponse reponse = mockMvc.perform(get("/api/v1/marques")).andReturn().getResponse();
        assertEquals(200, reponse.getStatus());
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"MarqueA\",\"dateCreation\":\"10-09-1923\"},{\"id\":2,\"nom\":\"MarqueB\",\"dateCreation\":\"23-10-1954\"}]", reponse.getContentAsString(),false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    void getMarqueByIdIT() throws Exception {
        MockHttpServletResponse reponse = mockMvc.perform(get("/api/v1/marques/2")).andReturn().getResponse();
        assertEquals(200, reponse.getStatus());
        try {
            JSONAssert.assertEquals("{\"id\":2,\"nom\":\"MarqueB\",\"dateCreation\":\"23-10-1954\"}", reponse.getContentAsString(),false);
        } catch (JSONException e) {
            fail();
        }
    }
    
    @Test
    void getMarqueByIdFailIT() throws Exception {
        int codeStatus = mockMvc.perform(get("/api/v1/marques/3000")).andReturn().getResponse().getStatus();
        assertEquals(404, codeStatus);
    }

    @Test
    void getMarqueByNameIT() throws Exception {
        MockHttpServletResponse reponse = mockMvc.perform(get("/api/v1/marques/MarqueA")).andReturn().getResponse();
        assertEquals(200, reponse.getStatus());
        try {
            JSONAssert.assertEquals("[{\"id\":1,\"nom\":\"MarqueA\"}]",reponse.getContentAsString(),false);
        } catch (JSONException e) {
            fail();
        }
    }

    @Test
    void createMarqueIT() throws Exception {
        MarqueDto dto = new MarqueDto(3L, "MarqueE",LocalDate.of(2024, 04, 16),"FFFFFF",null);
        MockHttpServletResponse reponse = mockMvc.perform(post("/api/v1/marques").contentType(MediaType.APPLICATION_JSON)
                                        .accept(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto)))
                                        .andReturn().getResponse();
        assertEquals(201, reponse.getStatus());
        try {
            JSONAssert.assertEquals("{\"id\":3,\"nom\":\"MarqueE\"}", reponse.getContentAsString(), false);
        } catch (JSONException e) {
            fail();
        }
    }
    
    @Test
    public void deleteMarqueByIdIT() throws Exception {
     int statusOk=mockMvc.perform(delete("/api/v1/marques/1")).andReturn().getResponse().getStatus();
     int status404=mockMvc.perform(delete("/api/v1/marques/3000")).andReturn().getResponse().getStatus();
     assertEquals(200, statusOk);
     assertEquals(404,status404);
    }
    
    @Test
    public void updateMarqueIT() throws JsonProcessingException, UnsupportedEncodingException, Exception {
        MarqueDto dto = new MarqueDto(2L, "MarqueBBB",LocalDate.of(2024,4,16),"FFFFFF",null);
        MockHttpServletResponse reponse = mockMvc
                .perform(put("/api/v1/marques/2")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto)))
                .andReturn().getResponse();
        assertEquals(200, reponse.getStatus());
        try {
            JSONAssert.assertEquals("{\"id\":2,\"nom\":\"MarqueBBB\",\"dateCreation\":\"16-04-2024\"}", reponse.getContentAsString(), false);
        } catch (JSONException e) {
            fail();
        }      
    }
}

