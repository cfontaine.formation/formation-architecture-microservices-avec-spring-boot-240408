USE formation;
DELIMITER $
CREATE PROCEDURE GET_COUNT_BY_PRIX(IN montant DOUBLE,OUT nb_article INT)
BEGIN
	SELECT count(id) INTO nb_article FROM articles WHERE prix<montant;
END $
DELIMITER ;

CALL GET_COUNT_BY_PRIX(80.0,@resultat);
SELECT @resultat;