package fr.dawan.sprinboot.entities;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "marques")
public class Marque extends BaseAuditing {

    private static final long serialVersionUID = 1L;

    @Column(length = 60, nullable = false)
    private String nom;

    @Column(name = "date_creation")
    private LocalDate dateCreation;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
    private CharteGraphique charteGraphique;

    @OneToMany(mappedBy = "marque", cascade = { CascadeType.PERSIST, CascadeType.REMOVE }) // ,fetch = FetchType.EAGER)
    @Exclude
    private Set<Article> articles = new HashSet<>();
}
