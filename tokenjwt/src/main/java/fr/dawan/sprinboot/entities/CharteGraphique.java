package fr.dawan.sprinboot.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "chartes_graphiques")
public class CharteGraphique extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(length = 6, nullable = false)
    private String couleur;

    @Lob
    @Column(length = 65000)
    @Exclude
    private byte[] logo;

    @OneToOne(mappedBy = "charteGraphique")
    @Exclude
    private Marque marque;
}
