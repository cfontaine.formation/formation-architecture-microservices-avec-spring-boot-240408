package fr.dawan.sprinboot.entities;

import java.util.HashSet;
import java.util.Set;

import fr.dawan.sprinboot.enums.Emballage;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "articles")
public class Article extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String description;

    private double prix;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    private Emballage emballage;

    @ManyToOne
    private Marque marque;

    @ManyToMany
    @Exclude
    private Set<Fournisseur> fournisseurs = new HashSet<>();
}
