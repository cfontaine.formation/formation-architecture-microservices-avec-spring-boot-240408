package fr.dawan.sprinboot.entities;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "fournisseurs")
public class Fournisseur extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Column(length = 60, nullable = false)
    private String nom;

    @ManyToMany(mappedBy = "fournisseurs")
    @Exclude
    private Set<Article> articles = new HashSet<>();

}
