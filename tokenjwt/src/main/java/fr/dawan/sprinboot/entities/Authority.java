package fr.dawan.sprinboot.entities;

import org.springframework.security.core.GrantedAuthority;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter

@Entity
@Table(name = "authorities_jwt")
public class Authority implements GrantedAuthority {

    private static final long serialVersionUID = 1L;

    @Id
    private String authority;

    @Override
    public String getAuthority() {
        return authority;
    }
}