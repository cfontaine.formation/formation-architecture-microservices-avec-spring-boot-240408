package fr.dawan.sprinboot.services;

import java.util.List;

import fr.dawan.sprinboot.dtos.CharteGraphiqueDto;

public interface CharteGraphiqueService extends GenericService<CharteGraphiqueDto, Long> {

    List<CharteGraphiqueDto> getByCouleur(String couleur);

}
