package fr.dawan.sprinboot.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jakarta.validation.Valid;

public interface GenericService<TDto, ID> {

    Page<TDto> getAll(Pageable page);

    Optional<TDto> getById(ID id);

    boolean delete(ID id);

    TDto save(@Valid TDto dto);

    Optional<TDto> update(@Valid  TDto dto, ID id);
}

