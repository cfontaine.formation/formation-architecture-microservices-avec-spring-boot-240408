package fr.dawan.sprinboot.services;

import java.util.List;

import fr.dawan.sprinboot.dtos.MarqueDto;

public interface MarqueService extends GenericService<MarqueDto, Long> {

    List<MarqueDto> getByNom(String modele);

}

