package fr.dawan.sprinboot.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.sprinboot.dtos.CharteGraphiqueDto;
import fr.dawan.sprinboot.entities.CharteGraphique;
import fr.dawan.sprinboot.repositories.CharteGraphiqueRepository;
import fr.dawan.sprinboot.services.CharteGraphiqueService;

@Service
@Transactional
public class CharteGraphiqueServiceImpl extends GenericServiceImpl<CharteGraphiqueDto, CharteGraphique, Long> implements CharteGraphiqueService {

    private CharteGraphiqueRepository repository;
    
    public CharteGraphiqueServiceImpl(CharteGraphiqueRepository repository,ModelMapper mapper) {
        super(repository, CharteGraphique.class, CharteGraphiqueDto.class,mapper);
        this.repository=repository;
    }

    @Override
    public List<CharteGraphiqueDto> getByCouleur(String couleur) {
        return repository.findByCouleur(couleur).stream().map(m -> mapper.map(m, CharteGraphiqueDto.class)).toList();
    }
    
    @Override
    public boolean delete(Long id) {
        return repository.removeById(id) != 0;
    }

    @Override
    protected void updateEntity(CharteGraphique cg, CharteGraphiqueDto dto) {
        cg.setCouleur(dto.getCouleur());
        cg.setLogo(dto.getLogo());
    }
}

