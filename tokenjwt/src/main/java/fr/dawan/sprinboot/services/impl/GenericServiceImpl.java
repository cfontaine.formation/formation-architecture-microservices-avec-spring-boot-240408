package fr.dawan.sprinboot.services.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import fr.dawan.sprinboot.services.GenericService;
import jakarta.validation.Valid;

@Validated
public abstract class GenericServiceImpl<TDto, T, ID> implements GenericService<TDto, ID> {

    private JpaRepository<T, ID> repository;

    private Class<T> clazz;

    private Class<TDto> clazzDto;
    
    protected ModelMapper mapper;

    public GenericServiceImpl(JpaRepository<T, ID> repository, Class<T> clazz, Class<TDto> clazzDto,ModelMapper mapper) {
        this.repository = repository;
        this.clazz = clazz;
        this.clazzDto = clazzDto;
        this.mapper=mapper;
    }

    @Override
    public Page<TDto> getAll(Pageable page) {
        Page<T> p = repository.findAll(page);
        List<TDto> lst = p.getContent().stream().map(e -> mapper.map(e, clazzDto)).toList();
        return new PageImpl<>(lst, p.getPageable(), p.getTotalElements());
    }

    @Override
    public Optional<TDto> getById(ID id) {
        return repository.findById(id).map(a -> Optional.of(mapper.map(a, clazzDto))).orElse(Optional.empty());
    }

    @Transactional
    @Override
    public TDto save(@Valid TDto dto) {
        System.out.println(dto);
        System.out.println(mapper.map(dto, clazz));
        return mapper.map(repository.saveAndFlush(mapper.map(dto, clazz)), clazzDto);
    }

    @Transactional
    @Override
    public Optional<TDto> update(@Valid TDto dto, ID id) {
        return Optional.of(repository.findById(id).map(a -> {
            updateEntity(a, dto);
            return mapper.map(repository.saveAndFlush(a), clazzDto);
        }).orElseGet(() -> mapper.map(repository.saveAndFlush(mapper.map(dto, clazz)), clazzDto)));
    }

    protected abstract void updateEntity(T entity, TDto dto);

}
