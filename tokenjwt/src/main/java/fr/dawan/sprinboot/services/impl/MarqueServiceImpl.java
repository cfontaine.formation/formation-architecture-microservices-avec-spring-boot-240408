package fr.dawan.sprinboot.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.sprinboot.dtos.MarqueDto;
import fr.dawan.sprinboot.entities.CharteGraphique;
import fr.dawan.sprinboot.entities.Marque;
import fr.dawan.sprinboot.repositories.MarqueRepository;
import fr.dawan.sprinboot.services.MarqueService;

@Service
@Transactional
public class MarqueServiceImpl extends GenericServiceImpl<MarqueDto, Marque, Long> implements MarqueService {

    private MarqueRepository repository;

    public MarqueServiceImpl(MarqueRepository repository,ModelMapper mapper) {
        super(repository, Marque.class, MarqueDto.class,mapper);
        this.repository = repository;
    }

    @Override
    public List<MarqueDto> getByNom(String modele) {
        return repository.findByNomLike(modele).stream().map(m -> mapper.map(m, MarqueDto.class)).toList();
    }

    @Override
    public boolean delete(Long id) {
        return repository.removeById(id) != 0;
    }

    @Override
    protected void updateEntity(Marque marque, MarqueDto dto) {
        if (marque.getCharteGraphique() == null) {
            marque.setCharteGraphique(new CharteGraphique());
            }
        marque.getCharteGraphique().setCouleur(dto.getCharteGraphiqueCouleur());
        marque.getCharteGraphique().setLogo(dto.getCharteGraphiqueLogo());
        marque.setNom(dto.getNom());
        marque.setDateCreation(dto.getDateCreation());
    }
}
