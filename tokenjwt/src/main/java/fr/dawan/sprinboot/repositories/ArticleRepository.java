package fr.dawan.sprinboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.sprinboot.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {

}
