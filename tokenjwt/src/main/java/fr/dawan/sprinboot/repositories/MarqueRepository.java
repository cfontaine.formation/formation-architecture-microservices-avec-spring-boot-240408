package fr.dawan.sprinboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.sprinboot.dtos.MarqueDto;
import fr.dawan.sprinboot.entities.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {

    List<MarqueDto> findByNomLike(String modele);
    
    int removeById(long id);

}
