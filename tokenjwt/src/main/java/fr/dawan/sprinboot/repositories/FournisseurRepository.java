package fr.dawan.sprinboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.sprinboot.entities.Fournisseur;

public interface FournisseurRepository extends JpaRepository<Fournisseur, Long>{

}
