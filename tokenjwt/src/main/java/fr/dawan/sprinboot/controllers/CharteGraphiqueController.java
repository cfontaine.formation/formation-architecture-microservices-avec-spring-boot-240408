package fr.dawan.sprinboot.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.sprinboot.dtos.CharteGraphiqueDto;
import fr.dawan.sprinboot.services.CharteGraphiqueService;

@RestController
@RequestMapping("/api/v1/chartes")
public class CharteGraphiqueController extends GenericController<CharteGraphiqueDto, Long> {

    private CharteGraphiqueService service;

    public CharteGraphiqueController(CharteGraphiqueService service) {
        super(service);
        this.service = service;
    }

    @GetMapping(value="/{couleur}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CharteGraphiqueDto> getByCouleur(@PathVariable String couleur) {
        return service.getByCouleur(couleur);
    }

    @PostMapping(value = "/logo/{id}", consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public CharteGraphiqueDto uploadImage(@PathVariable long id, @RequestParam("image") MultipartFile file) throws IOException {
        CharteGraphiqueDto cg = service.getById(id).get();
        cg.setLogo(file.getBytes());
        return service.save(cg);
    }

}
