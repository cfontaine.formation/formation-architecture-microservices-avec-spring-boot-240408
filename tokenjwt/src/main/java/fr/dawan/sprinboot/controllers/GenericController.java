package fr.dawan.sprinboot.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import fr.dawan.sprinboot.services.GenericService;

public abstract class GenericController<TDto, ID> {

    private GenericService<TDto, ID> service;

    public GenericController(GenericService<TDto, ID> service) {
        this.service = service;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TDto> getAll(Pageable page) {
        return service.getAll(page).getContent();
    }

    @GetMapping(value = "/{id:[0-9]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TDto> getById(@PathVariable ID id) {
        Optional<TDto> dto = service.getById(id);
        if (dto.isPresent()) {
            return ResponseEntity.ok(dto.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> delete(@PathVariable ID id) {
        if (service.delete(id)) {
            return new ResponseEntity<>(id + " est supprimée ", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(id + " n'existe pas ", HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public TDto create(@RequestBody TDto dto) {
        return service.save(dto);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TDto> update(@PathVariable ID id, @RequestBody TDto dto) {
        Optional<TDto> d = service.update(dto, id);
        if (d.isPresent()) {
            return ResponseEntity.ok(d.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
