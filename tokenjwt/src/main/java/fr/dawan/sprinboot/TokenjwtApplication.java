package fr.dawan.sprinboot;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fr.dawan.sprinboot.dtos.MarqueDto;
import fr.dawan.sprinboot.entities.Marque;

@SpringBootApplication
public class TokenjwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokenjwtApplication.class, args);
	}
	 @Bean
	    ModelMapper modelmapper() {
	        ModelMapper mapper = new ModelMapper();
	        mapper.getConfiguration().setAmbiguityIgnored(true);
	        mapper.typeMap(MarqueDto.class, Marque.class)
	        .addMappings(m -> m.map(src -> src.getCharteGraphiqueCouleur(), (dest, v) -> dest.getCharteGraphique().setCouleur((String) v)))
	        .addMappings(m -> m.map(src -> src.getCharteGraphiqueLogo(), (dest, v) -> dest.getCharteGraphique().setLogo((byte[]) v)));
	        return mapper;
	    }

}
