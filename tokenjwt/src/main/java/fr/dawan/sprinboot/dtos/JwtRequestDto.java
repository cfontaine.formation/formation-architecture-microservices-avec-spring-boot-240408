package fr.dawan.sprinboot.dtos;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class JwtRequestDto {

    @NotEmpty
    @Size(min = 1, max = 150)
    private String email;

    @NotEmpty
    @Size(min = 8, max = 40)
    private String password;

}
