package fr.dawan.sprinboot.dtos;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ApiError {

    private HttpStatus code;

    private String message;

    @Setter(AccessLevel.NONE)
    private LocalDateTime timeStamp = LocalDateTime.now();
}
