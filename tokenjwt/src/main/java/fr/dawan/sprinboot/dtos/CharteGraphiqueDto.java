package fr.dawan.sprinboot.dtos;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class CharteGraphiqueDto {

    private long id;
    
    @NotEmpty
    @Size(min = 6, max = 6)
    @Pattern(regexp = "[0-9a-fA-F]+")
    private String couleur;

    private byte[] logo;
}
