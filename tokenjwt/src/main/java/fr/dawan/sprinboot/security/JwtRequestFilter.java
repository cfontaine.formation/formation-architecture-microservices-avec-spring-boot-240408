package fr.dawan.sprinboot.security;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    private UserDetailsService userDetailService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String requestTokenHeader=request.getHeader("Authorization");
        if(requestTokenHeader!=null &&requestTokenHeader.startsWith("Bearer ")) {
            String jwtToken=requestTokenHeader.substring(7);
            String username=jwtTokenUtil.getUsername(jwtToken);
            if(username!=null && SecurityContextHolder.getContext().getAuthentication()==null) {
                UserDetails userDetails=userDetailService.loadUserByUsername(username);
                if(jwtTokenUtil.valideJwtToken(jwtToken, userDetails)) {
                    UsernamePasswordAuthenticationToken userPasswordAuthToken= new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                    userPasswordAuthToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(userPasswordAuthToken);
                }
            }
        }
        filterChain.doFilter(request, response);
    }

}
