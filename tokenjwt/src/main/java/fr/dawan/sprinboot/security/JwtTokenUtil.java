package fr.dawan.sprinboot.security;

import java.util.Date;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.lang.Function;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtTokenUtil {
    
    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private int expiration;

    public String generateJwtToken(UserDetails userDetail) {
        return Jwts.builder()
                .claims()
                    // TODO reactiver les authorithies
                   /* .empty().add("authorities", userDetail.getAuthorities())*/
                    .subject(userDetail.getUsername()).issuedAt(new Date())
                    .expiration(new Date((new Date()).getTime() + expiration))
                .and()
                .signWith(getSecretKey(), Jwts.SIG.HS256).compact();
    }

    public boolean valideJwtToken(String token, UserDetails userDetail) {
        return getUsername(token).equals(userDetail.getUsername()) && !isTokenExpired(token);
    }

    public String getUsername(String token) {
        return getClaim(token, Claims::getSubject);
    }

    public boolean isTokenExpired(String token) {
        return getExpiration(token).before(new Date());
    }

    public Date getExpiration(String token) {
        return getClaim(token, Claims::getExpiration);
    }

    private <T> T getClaim(String token, Function<Claims, T> claimsResolver) {
        return claimsResolver.apply(extractAllClaims(token));
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().verifyWith(getSecretKey()).build().parseSignedClaims(token).getPayload();
    }

    private SecretKey getSecretKey() {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(secret));
    }
}

