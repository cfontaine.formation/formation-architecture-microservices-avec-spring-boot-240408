package fr.dawan.monument;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fr.dawan.monument.dtos.MonumentDto;
import fr.dawan.monument.entities.Monument;

@SpringBootApplication
public class MonumentApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonumentApplication.class, args);
    }

    @Bean
    ModelMapper modelmapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.typeMap(MonumentDto.class, Monument.class).addMappings(m -> {
            m.map(src -> src.getCoordonneLatitude(), (dest, v) -> dest.getCoordonne().setLatitude((Double) v));
            m.map(src -> src.getCoordonneLongitude(), (dest, v) -> dest.getCoordonne().setLongitude((Double) v));

        });
        return mapper;
    }

}
