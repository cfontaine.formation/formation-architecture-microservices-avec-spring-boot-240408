package fr.dawan.monument.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.monument.dtos.MonumentDto;
import fr.dawan.monument.entities.Monument;
import fr.dawan.monument.entities.repositories.MonumentRepository;
import fr.dawan.monument.services.MonumentService;
@Service
@Transactional
public class MonumentServiceImpl implements MonumentService {

    private MonumentRepository repository;
    
    private ModelMapper mapper;

    public MonumentServiceImpl(MonumentRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<MonumentDto> getAll(Pageable page) {
        return repository.findAll(page).stream().map(m -> mapper.map(m,MonumentDto.class)).toList();
    }

    @Transactional(readOnly = true)
    @Override
    public MonumentDto getById(long id) {
        return mapper.map(repository.findById(id), MonumentDto.class);
    }

    @Override
    public boolean deleteById(long id) {
        return repository.removeById(id)!=0;
    }

    @Override
    public MonumentDto save(MonumentDto dto) {
        return mapper.map(repository.saveAndFlush(mapper.map(dto, Monument.class)), MonumentDto.class);
    }

    @Override
    public MonumentDto update(MonumentDto dto, long id) {
      Monument m=repository.findById(id).get();
      m.setId(dto.getId());
      m.setNom(dto.getNom());
      m.setAnneeConstruction(dto.getAnneeConstruction());
      m.setDescription(dto.getDescription());
      m.setPhoto(dto.getPhoto());
      m.getLocalisation().setId(dto.getLocalisationId());
      return mapper.map(repository.saveAndFlush(m), MonumentDto.class);
    }

}
