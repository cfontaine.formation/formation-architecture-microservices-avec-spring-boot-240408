package fr.dawan.monument.services;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.monument.dtos.MonumentDto;

public interface MonumentService {
    List<MonumentDto> getAll(Pageable page);
    
    MonumentDto getById(long id);
    
    boolean deleteById(long id);
    
    MonumentDto save(MonumentDto dto);
    
    MonumentDto update(MonumentDto dto,long id);
}
