package fr.dawan.monument.entities.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.dawan.monument.entities.Etiquette;
import fr.dawan.monument.entities.Localisation;

public interface LocalisationRepository extends JpaRepository<Localisation, Long> {

    @Query("FROM Localisation l WHERE l.ville=:ville")
    List<Localisation> findByNomVille(String ville);
}
