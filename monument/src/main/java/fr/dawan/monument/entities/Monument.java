package fr.dawan.monument.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

@Entity
@Table(name = "monuments")
public class Monument implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;

    @Column(nullable = false, length = 150)
    private String nom;

    @Column(name = "annee_construction")
    private int anneeConstruction;

    private String description;

    @Lob
    @Column(length = 65000)
    @Exclude
    private byte[] photo;

    @OneToOne(cascade = CascadeType.PERSIST)
    @Exclude
    @Setter(value=AccessLevel.NONE)
    private Coordonne coordonne;

    @ManyToOne
    @Exclude
    private Localisation localisation;

    @ManyToMany
    @Exclude
    private Set<Etiquette> etiquettes = new HashSet<>();
}
