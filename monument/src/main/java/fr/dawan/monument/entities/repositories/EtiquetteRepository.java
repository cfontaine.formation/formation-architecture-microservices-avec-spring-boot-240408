package fr.dawan.monument.entities.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.dawan.monument.entities.Etiquette;

public interface EtiquetteRepository extends JpaRepository<Etiquette, Long> {
    //  Rechercher les étiquettes en fonction d'un modèle(LIKE) sur l'intitule' de l’étiquette
    @Query("FROM Etiquette e WHERE e.intitule like :modele ")
    List<Etiquette> findByIntituleLike(String modele);

}
