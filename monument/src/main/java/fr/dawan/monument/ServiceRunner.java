package fr.dawan.monument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.monument.dtos.MonumentDto;
import fr.dawan.monument.services.MonumentService;
@Component
public class ServiceRunner implements CommandLineRunner {
    @Autowired
    MonumentService service;

    @Override
    public void run(String... args) throws Exception {
        service.getAll(Pageable.unpaged()).forEach(System.out::println);
        MonumentDto dto = MonumentDto.builder().nom("Opéra Garnier")
                .coordonneLatitude(48.871257).coordonneLongitude(2.331955).localisationId(2)
                .description("opéra Garniera, ou palais Garniera, est un théâtre national qui a la vocation d'être une académie de musique, de chorégraphie et de poésie lyrique")
                .build();
        dto=service.save(dto);
        System.out.println(service.getById(dto.getId()));
        dto.setAnneeConstruction(1875);
        dto=service.update(dto, dto.getId());
        System.out.println(dto);
        service.getAll(Pageable.unpaged()).forEach(System.out::println);
    }

}
