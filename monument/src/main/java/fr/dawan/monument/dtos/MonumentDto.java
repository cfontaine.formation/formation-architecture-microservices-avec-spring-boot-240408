package fr.dawan.monument.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class MonumentDto {
    private long id;
    private String nom;
    private int anneeConstruction;
    private String description;
    @Exclude
    private byte[] photo;
    private double coordonneLatitude;
    private double coordonneLongitude;
    private long localisationId;
}
