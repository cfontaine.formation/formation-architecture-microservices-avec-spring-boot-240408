package fr.dawan.springcore;

import java.time.LocalDate;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.Article;
import fr.dawan.springcore.beans.DtoMapper;
import fr.dawan.springcore.components.ArticleRepository;
import fr.dawan.springcore.components.ArticleService;

public class App {
    public static void main(String[] args) {
        // Exemple lombok
        Article a = new Article("TV", 400.0, LocalDate.of(2024, 04, 8));
        a = new Article(LocalDate.of(2024, 04, 8));
        System.out.println(a);
        // a.setDateCreation(null); // @NonNull -> entraîne la génération de
        // vérification par rapport à la valeur null
        System.out.println(a);

        // @Builder
        Article a2 = Article.builder().prix(10.0).dateCreation(LocalDate.of(2024, 04, 02)).build();
        System.out.println(a2);

        // Spring Core
        // Création du conteneur d'ioc
        ApplicationContext cioc = new AnnotationConfigApplicationContext(AppConf.class);
        System.out.println("---------------------------------------");
        // getBean -> permet de récupérer les instances des beans depuis le conteneur
        DtoMapper m1 = cioc.getBean("mapper1bis", DtoMapper.class);
        System.out.println(m1);

        ArticleRepository repo1 = cioc.getBean("repository1", ArticleRepository.class);
        System.out.println(repo1);

        ArticleRepository repo2 = cioc.getBean("repository2", ArticleRepository.class);
        System.out.println(repo2);

        ArticleService service1 = cioc.getBean("service1", ArticleService.class);
        System.out.println(service1);

        // Fermeture du context entraine la destruction de tous les beans
        ((AbstractApplicationContext) cioc).close();
    }
}
