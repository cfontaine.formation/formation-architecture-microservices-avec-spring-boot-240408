package fr.dawan.springcore.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.dawan.springcore.beans.DtoMapper;

@Service("service1")
public class ArticleService {

    @Autowired
    @Qualifier("repository1")
    // Injection automatique de la dépendence. Un bean de type ArticleRepository est
    // recherché dans le conteneur d'ioc et il est injecté dans la variable
    // d'instance repository
    // s'il y a plusieurs bean une exception est générée, à moins de lever
    // l'ambiguité avec @Primary ou @Qualifier

    // @Autowired(required = false)
    // required = false-> dépendence optionnelle: s'il n'y a pas de bean de type
    // ArticleRepository dans le conteneur -> repository= null et pas d'exception
    private ArticleRepository repository;

    // @Autowired -> @Autowired peut être placé sur les variables d'instances
    // injection en utilisant la réflexion
    private DtoMapper mapper;

    public ArticleService() {
        System.out.println("Constructeur par défaut ArticleService");
    }

    // @Autowired peut être placé sur le constructeur uniquement, s'il y en a
    // plusieurs
    // pour indiquer celui qui doit être utilisé.
    // s'il y a un constructeur, il n'est pas nécessaire.
    @Autowired
    public ArticleService(@Qualifier("repository2") ArticleRepository repository, DtoMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
        System.out.println("Constructeur 2 paramètre ArticleService");
    }

    public ArticleRepository getRepository() {
        return repository;
    }

    // @Autowired -> @Autowired peut être placé sur les setter
    public void setRepository(/* @Qualifier("repository2") */ArticleRepository repository) {
        this.repository = repository;
        System.out.println("Setter ArticleService");
    }

    public DtoMapper getMapper() {
        return mapper;
    }

    // @Autowired
    public void setMapper(DtoMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public String toString() {
        return "ArticleService [repository=" + repository + ", mapper=" + mapper + "]";
    }

}
